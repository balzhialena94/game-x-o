import './App.css';
import React from 'react';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      squares: [],
      count : 0,
    }
    this.winnerLine = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 4, 8],
      [2, 4, 6],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8]
    ]
    this.clickHandler = this.clickHandler.bind(this);
    this.start = this.start.bind(this);
    this.isWinner = this.isWinner.bind(this)
  }

isWinner() {
    const s = (this.state.count % 2 === 0) ? 'X' : '0';
    const {squares} = this.state;
    let hasWinner = false;
   
  
    for(let i = 0; i < 8 ; i++){
      let line = this.winnerLine[i];
      console.log(s )
      if (squares[line[0]] === s &&
        squares[line[1]] === s &&
        squares[line[2]] === s) {
          hasWinner = true;
          alert('выиграш ' + s);
          this.start();
          break;
        }    
    }
      
    if(!hasWinner && this.state.count === 8 ) {
      alert('ничья');
      this.start();
    }
  }
  
  start(){
    this.setState({
      squares: [],
      count : 0
    })
  }
  
  clickHandler({target: {id}}){
    let currentSquares = this.state.squares;
    
    if (currentSquares[id]) {
      alert('Wrong way');
      return 0;
    }
    currentSquares[id] = (this.state.count % 2 === 0) ? 'X' : '0';
      this.setState({
        count: this.state.count +1,
        squares : currentSquares
      });
      this.isWinner();
    
  }



  render() {
    const status = 'Next player: ' + (this.state.count % 2 ? 'O' : 'X');


  
  return (
      <div className='status'> {status} 
      <div className="tic-tac">
        <div className="ttt" onClick={this.clickHandler} id = "0">{this.state.squares[0]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "1">{this.state.squares[1]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "2">{this.state.squares[2]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "3">{this.state.squares[3]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "4">{this.state.squares[4]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "5">{this.state.squares[5]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "6">{this.state.squares[6]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "7">{this.state.squares[7]}</div>
        <div className="ttt" onClick={this.clickHandler} id = "8">{this.state.squares[8]}</div>
        <div>
          <button className= "newGame" onClick={this.start}> новая игра</button>

      </div>
      </div>
      </div>
  );
  }
}

export default App;
